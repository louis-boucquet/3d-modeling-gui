import React from "react";
import { Coordinate } from "./types";

type Props = {
	from: Coordinate,
	to: Coordinate,
}

type State = {
}

export default class ConnectionComponent extends React.Component<Props, State> {
	constructor(props: Props) {
		super(props);

		this.state = {}
	}

	render() {
		const avgX = (this.props.from.x + this.props.to.x) / 2;
		const pathString = `
		M ${this.props.from.x} ${this.props.from.y}
		C ${avgX} ${this.props.from.y},
			${avgX} ${this.props.to.y},
			${this.props.to.x} ${this.props.to.y}`;

		return <path d={pathString} stroke="black" fill="transparent" />;
	}
}
