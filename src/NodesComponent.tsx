import React from "react";

import NodeComponent from "./NodeComponent";
import { Coordinate, Input, Node, Source, Tree } from "./types";
import "./svg.css";
import ConnectionComponent from "./ConnectionComponent";

export type ConnectionInProgress = {
	source?: {
		node: Node,
		outputNumber: number,
	},
	target?: Input,
	from: Coordinate,
	to: Coordinate,
}

type Props = {
	root: Node,
	onTreeChange: (t: Tree<null>) => void,
}

type State = {
	nodes: Set<Node>,
	connection?: ConnectionInProgress,
}

export default class NodesComponent extends React.Component<Props, State> {
	constructor(props: Props) {
		super(props);

		const nodes = new Set<Node>();

		nodes.add(props.root);

		this.updateTree();

		this.state = { nodes }
	}

	updateTree() {
		const tree = this.buildTreeFromNode(this.props.root);
		this.props.onTreeChange(tree);
	}

	private buildTreeFromNode(node: Node) {
		const tree: Tree<any> = {
			value: node.fields,
			connections: {},
		}

		for (const input of node.inputs)
			if (input.source)
				tree.connections[input.name] = this.buildTreeFromNode(input.source.node);

		return tree;
	}

	startConnection(connection: ConnectionInProgress) {
		if (!this.state.connection)
			this.setState({ connection });
	}

	finishConnectionWithInput(target: Input) {
		if (this.state.connection && this.state.connection.source)
			target.source = this.state.connection.source;

		this.setState({ connection: undefined });
		this.updateTree();
	}

	finishConnectionWithSource(source: Source) {
		if (this.state.connection && this.state.connection.target) {
			const target = this.state.connection.target
			target.source = source;
		}

		this.setState({ connection: undefined });
		this.updateTree();
	}

	updateNodes() {
		this.setState({ nodes: this.state.nodes });
	}

	addNode() {
		const node: Node = {
			fields: {},
			fieldsConfig: [],
			position: { x: 400, y: 200 },
			size: {
				x: 150,
				y: 100,
			},
			inputs: [
				{
					name: "Input1",
					type: "typeA",
				},
				{
					name: "Input2",
					type: "typeB",
				},
			],
			outputs: [
				{
					name: "Output2",
					type: "typeA",
				},
				{
					name: "Output2",
					type: "typeB",
				},
			],
		}

		this.state.nodes.add(node);
		this.updateNodes();

		this.updateTree();
	}

	render() {
		const nodes = [...this.state.nodes]
			.map(node => this.toNodeComponent(node));

		return <div>
			<button
				onClick={e => this.addNode()}
			>Add Node</button>
			<svg
				onMouseMove={e => {
					if (this.state.connection) {
						const c = this.state.connection;
						c.to.x += e.movementX;
						c.to.y += e.movementY;

						this.setState({ connection: c });
					}
				}}
				onMouseUp={e => {
					this.setState({ connection: undefined });
				}}
				width={window.innerWidth}
				height={window.innerHeight}
			>
				{nodes}
				{this.state.connection && <ConnectionComponent
					from={this.state.connection.from}
					to={this.state.connection.to}
				/>}
			</svg>;
		</div>
	}

	toNodeComponent(node: Node) {
		return <NodeComponent
			startConnection={c => this.startConnection(c)}
			updateFields={() => {
				this.updateNodes();
				this.updateTree();
			}}
			finishConnectionWithInput={c => this.finishConnectionWithInput(c)}
			finishConnectionWithSource={c => this.finishConnectionWithSource(c)}
			onDrag={(to: Coordinate) => {
				node.position = to;
				this.updateNodes();
			}}
			onResize={(newSize: Coordinate) => {
				node.size = newSize;
				this.updateNodes();
			}}
			node={node}
		/>
	}
}