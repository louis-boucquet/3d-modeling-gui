export type Coordinate = {
	x: number,
	y: number,
}

export type Type = "number" | "boolean" | "string";

export type InputField = {
	type: Type,
	name: string,
};

export type Source = {
	node: Node,
	outputNumber: number,
}

export type Input = {
	name: string,
	type: string,
	source?: Source,
}

export type Output = {
	name: string,
	type: string,
}

export type NodeConfig = {
	fields: InputField[],
	size: Coordinate,
	inputs: Input[],
	outputs: Output[],
}

export type Node = {
	fields: { [name: string]: any }
	fieldsConfig: InputField[],
	position: Coordinate,
	size: Coordinate,
	inputs: Input[],
	outputs: Output[],
}

export type Tree<T> = {
	value: T,
	connections: { [key: string]: Tree<T> },
}
