import React from "react";
import ConnectionComponent from "./ConnectionComponent";
import { ConnectionInProgress } from "./NodesComponent";
import { Coordinate, Input, Node, Source } from "./types";

type Props = {
	node: Node,
	onDrag: (to: Coordinate) => void,
	onResize: (newSize: Coordinate) => void,
	updateFields: () => void,
	startConnection: (c: ConnectionInProgress) => void,
	finishConnectionWithInput: (n: Input) => void,
	finishConnectionWithSource: (s: Source) => void,
}

type State = {
	dragging: boolean,
	resizing: boolean,
}

export default class NodeComponent extends React.Component<Props, State> {
	constructor(props: Props) {
		super(props);

		this.state = {
			dragging: false,
			resizing: false,
		}
	}

	componentDidMount() {
		document.addEventListener("mousemove", e => this.onMouseMove(e));
	}

	onMouseMove(e: MouseEvent) {
		e.preventDefault();
		if (this.state.dragging)
			this.props.onDrag({
				x: this.props.node.position.x + e.movementX,
				y: this.props.node.position.y + e.movementY,
			});
		if (this.state.resizing)
			this.props.onResize({
				x: this.props.node.size.x + e.movementX,
				y: this.props.node.size.y + e.movementY,
			});
	}

	onMouseDownDrag(e: React.MouseEvent<SVGRectElement, MouseEvent>) {
		this.setState({ dragging: true });
	}

	onMouseUpDrag(e: React.MouseEvent<SVGRectElement, MouseEvent>) {
		this.setState({ dragging: false });
	}

	onMouseDownResize(e: React.MouseEvent<SVGRectElement, MouseEvent>) {
		this.setState({ resizing: true });
	}

	onMouseUpResize(e: React.MouseEvent<SVGRectElement, MouseEvent>) {
		this.setState({ resizing: false });
	}

	render() {
		const inputElements = this.props.node.inputs.map((input, i) => {
			const location = {
				x: this.props.node.position.x,
				y: this.props.node.position.y + 20 + 15 + 30 * i,
			}
			const elements = [
				<circle
					onMouseDown={e => {
						this.props.startConnection({
							target: input,
							from: Object.assign({}, location),
							to: Object.assign({}, location),
						});
					}}
					onMouseUp={e => {
						this.props.finishConnectionWithInput(input);
					}}
					cx={location.x}
					cy={location.y}
					r="6"
					fill="#999"
					stroke="#ddd"
				/>,
				<text
					x={this.props.node.position.x + 10}
					y={this.props.node.position.y + 20 + 15 + 6 + 30 * i}
				>{input.name}</text>,
			];

			const connection = this.inputConnection(input, i);
			if (connection)
				elements.push(connection);

			return elements;
		}).reduce((tot, n) => tot.concat(n), []);
		const outputElements = this.props.node.outputs.map((output, i) => {
			const location = {
				x: this.props.node.position.x + this.props.node.size.x,
				y: this.props.node.position.y + 20 + 15 + 30 * i,
			}
			return [
				<circle
					onMouseDown={e => {
						this.props.startConnection({
							source: {
								outputNumber: i,
								node: this.props.node,
							},
							from: Object.assign({}, location),
							to: Object.assign({}, location),
						});
					}}
					onMouseUp={e => {
						this.props.finishConnectionWithSource({
							outputNumber: i,
							node: this.props.node,
						});
					}}
					cx={this.props.node.position.x + this.props.node.size.x}
					cy={this.props.node.position.y + 20 + 15 + 30 * i}
					r="5"
					fill="#999"
					stroke="#ddd"
				/>,
				<text
					x={this.props.node.position.x + this.props.node.size.x - 10}
					y={this.props.node.position.y + 20 + 15 + 6 + 30 * i}
					textAnchor="end"
				>{output.name}</text>,
			]
		}).reduce((tot, n) => tot.concat(n), []);
		return [
			...this.mainElements(),
			...inputElements,
			...outputElements,
		];
	}

	mainElements() {
		const padding = 20 + 15 + 30 * Math.max(this.props.node.inputs.length, this.props.node.outputs.length);
		return [
			// background
			<rect
				onMouseDown={e => this.onMouseDownDrag(e)}
				onMouseUp={e => this.onMouseUpDrag(e)}
				x={this.props.node.position.x}
				y={this.props.node.position.y}
				width={this.props.node.size.x}
				height={this.props.node.size.y}
				rx="10"
				fill="#bbb"
			/>,
			// none node inputs
			<foreignObject
				x={this.props.node.position.x}
				y={this.props.node.position.y + padding}
				width={this.props.node.size.x}
				height={this.props.node.size.y - padding}
			>
				{this.inputFields()}
			</foreignObject>,
			// header
			<rect
				onMouseDown={e => this.onMouseDownDrag(e)}
				onMouseUp={e => this.onMouseUpDrag(e)}
				x={this.props.node.position.x}
				y={this.props.node.position.y}
				width={this.props.node.size.x}
				height="20"
				rx="10"
				fill="#999"
			/>,
			// resize handle
			<rect
				onMouseDown={e => this.onMouseDownResize(e)}
				onMouseUp={e => this.onMouseUpResize(e)}
				x={this.props.node.position.x + this.props.node.size.x - 10}
				y={this.props.node.position.y + this.props.node.size.y - 10}
				width="10"
				height="10"
				fill="#999"
			/>,
		]
	}

	inputFields() {
		return this
			.props
			.node
			.fieldsConfig
			.map(config => <input
				type={config.type}
				value={this.props.node.fields[config.name]}
				onChange={e => {
					this.props.node.fields[config.name] = e.target.value;
					this.props.updateFields();
					console.log(this.props.node.fields);
				}}
			/>);
	}

	inputConnection(input: Input, i: number) {
		if (!input.source)
			return;
		return <ConnectionComponent
			from={{
				x: input.source.node.position.x + input.source.node.size.x,
				y: input.source.node.position.y + 20 + 15 + 30 * i,
			}}
			to={{
				x: this.props.node.position.x,
				y: this.props.node.position.y + 20 + 15 + 30 * i,
			}}
		/>;
	}
}
