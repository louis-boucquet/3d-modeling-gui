import NodesComponent from "./NodesComponent";

export default function App() {
	return <NodesComponent
		onTreeChange={tree => {
			console.log(tree);
		}}
		root={{
			fields: {
				test: 0,
			},
			fieldsConfig: [
				{
					type: "number",
					name: "test",
				}
			],
			position: { x: 700, y: 150 },
			size: {
				x: 150,
				y: 150,
			},
			inputs: [
				{
					name: "Input1",
					type: "typeA",
				},
				{
					name: "Input2",
					type: "typeB",
				},
			],
			outputs: [],
		}}
	/>;
}
